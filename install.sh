#!/bin/bash

pwd = $PWD
apt install apache2 php7 libapache2 python3 python3-pip
pip install bluepy

cp html/* /var/www/html

ln -s $PWD/sense-server.service /etc/systemd/system/sense-server.service

./boardscan.py -o boards.txt

systemctl enable sense-server.service
systemctl start sense-server.service
