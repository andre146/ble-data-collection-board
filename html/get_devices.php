<?php

ignore_user_abort(False);

$tcp_ip = $_SERVER['SERVER_ADDR'];
$tcp_port = 56254;

$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if(!socket_connect($sock, $tcp_ip, $tcp_port)){
    print("Fehler beim verbinden der Socket!");
    ob_flush();
    flush();
    die();
}

socket_write($sock, "DEVICES?");
socket_set_block($sock);
$result = socket_read($sock, 1024, PHP_NORMAL_READ);

if($result != ""){
    print($result);
}

socket_close($sock);

?>
