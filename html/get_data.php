<?php
header("Content-Type: text/event-stream");
ignore_user_abort(False);

$device = $_GET['device'];
$tcp_ip = $_SERVER['SERVER_ADDR'];
$tcp_port = 56254;

$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if(!socket_connect($sock, $tcp_ip, $tcp_port)){
    print("Fehler beim verbinden der Socket!");
    ob_flush();
    flush();
    die();
}

socket_write($sock, "READ " . $device);
while(True){
    $result = socket_read($sock, 1024);

    if($result != ""){
        print("data: " . $result. "\n\n");
    }
    ob_flush();
    flush();

    if(connection_aborted()){
        die();
    }

}

socket_close($sock);

?>
