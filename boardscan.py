#!/usr/bin/python3
from bluepy.btle import Scanner
import argparse

parser = argparse.ArgumentParser(description="Search for any Data Collection Boards V1.0" )
parser.add_argument("-t", "--timeout", default=5, type=int, help="Set the scan timeout in seconds")
parser.add_argument("-o", "--output", type=str, help="Output the list of available boards to a specified file")
args = parser.parse_args()
timeout = args.timeout
out_file_path = args.output

scanner = Scanner()
devices = scanner.scan(timeout)
board_list = []

for dev in devices:
    for (ad_type, desc, value) in dev.getScanData():
        if(value.find("BKT Data Collection Board") != -1):
            board_list.append(dev.addr)

print("Available Boards:")
for board in board_list:
    print(board)

if out_file_path != None:
    out_file = open(out_file_path, "w")
    for board in board_list:
        out_file.write(board + "\n")

    out_file.close()