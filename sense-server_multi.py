#!/usr/bin/python3

from bluepy.btle import UUID, Peripheral, Scanner
import struct
import time
import socket
import threading
import argparse

#device address
devices_avail = []
devices_connected = {}
#services
imu_uuid = "039343e0-7372-465c-9a6f-ffa139d18cfe"
hts_uuid = "04bd859d-d38a-4d8b-8314-3785accacd55"
util_uuid = "35cca5a5-ec0c-43ec-9cfe-3831f1ac3793"
#characteristics
acc_uuid_x = "039343e1-7372-465c-9a6f-ffa139d18cfe"
acc_uuid_y = "039343e2-7372-465c-9a6f-ffa139d18cfe"
acc_uuid_z = "039343e3-7372-465c-9a6f-ffa139d18cfe"

flux_uuid_x = "039343e4-7372-465c-9a6f-ffa139d18cfe"
flux_uuid_y = "039343e5-7372-465c-9a6f-ffa139d18cfe"
flux_uuid_z = "039343e6-7372-465c-9a6f-ffa139d18cfe"

temp_uuid = "04bd859e-d38a-4d8b-8314-3785accacd55"
time_uuid = "35cca5a7-ec0c-43ec-9cfe-3831f1ac3793"
batt_uuid = "35cca5a6-ec0c-43ec-9cfe-3831f1ac3793"
csv_uuid = "35cca5a8-ec0c-43ec-9cfe-3831f1ac3793"

#TCP settings
tcp_port = 56254

verbose_output = False

# Scan for available boards
def discover_boards(timeout=5):
	global devices_avail
	devices_found = []
	scanner = Scanner()
	devices = scanner.scan(timeout)

	for dev in devices:
		for (ad_type, desc, value) in dev.getScanData():
			if(value.find("BKT Data Collection Board") != -1):
				devices_found.append(dev.addr)

	return(devices_found)

# Try to connect to a device and retry if connection failed
def ble_connect(dev_addr=None):
	global devices_connected

	try:
		device = Peripheral(dev_addr, "public")
		print("[BLE] Connected to device", dev_addr)
	except Exception as e:
		print("[BLE] Could not connect to device", dev_addr)
		print("[DEBUG] Exception: ", e)
		return False

	# setup all the available services
	util_service = device.getServiceByUUID(UUID(util_uuid))

	# setup all characteristics
	time_char = util_service.getCharacteristics(time_uuid)[0]

	# sync board time to local time
	time_char.write(struct.pack("=l", round(time.time())), withResponse=True)
	board_time = struct.unpack("=l", time_char.read())[0]
	if (time.time() - board_time) > 2:
		print("[BLE] Error syncing board time to local time!")
		print("[BLE] Board time is", board_time, ",Local time is", round(time.time()))
		device.disconnect()
		return False
	else:
		print("[BLE] Successfully synced board time to local time")

	devices_connected[dev_addr] = ""
	return(device)

# handles incoming TCP connections
def connection_handler(conn, addr):
	global devices_connected
	global active_connections
	line_buffer_old = ""

	recv_data = conn.recv(1024).decode('utf-8')

	# Scan for available devices and send the list back to the client
	if(recv_data.find("DEVICES?") > -1):
		send_string = "DEVICES:"
		for dev in devices_avail:
			send_string += " " + str(dev)

		try:
			conn.send((send_string + "\n").encode('utf-8'))
		except:
			print("[TCP] Failed to report available devices to client!")

	# Read data from the BLE device in an infinite loop and send it to the client
	elif(recv_data.find("READ") > -1):
		device_addr = recv_data.split(" ")[1].strip("\n")

		if(device_addr == ""):
			conn.close()
			return
		else:
			connected_flag = False
			for dev in devices_connected.keys():
				if dev == device_addr:
					connected_flag = True

			if not connected_flag:
				# start the value reader in a separate thread
				value_reader_thread = threading.Thread(target=ble_value_reader, args=(device_addr,), daemon=True)
				value_reader_thread.start()
		try:
			#conn.setblocking(0)
			conn.send(str("Timestamp; Acc X [g]; Acc Y [g]; Acc Z [g]; Flux X [µT]; Flux Y [µT]; Flux Z [µT]; Temp [°C]\n").encode('utf-8'))

			while True:
				connected_flag = False
				for dev in devices_connected.keys():
					if dev == device_addr:
						connected_flag = True
				if connected_flag:
					if line_buffer_old != devices_connected[device_addr]:
						line_buffer_old = devices_connected[device_addr]
						conn.send(str(line_buffer_old).encode('utf-8'))

				time.sleep(0.5)

		except Exception as e:
			pass

	print("[TCP] Connection to", addr, "ended. Terminating handler")
	conn.close()

# reads values from the device over BLE
def ble_value_reader(device_addr):
	global devices_connected
	global devices_avail
	device = ""
	connected_flag = False
	for dev in devices_connected.keys():
		if dev == device_addr:
			connected_flag = True

	if not connected_flag:
		# check if the supplied device address exists in the list of discovered devices
		device_addr_valid = False
		for dev in devices_avail:
			if dev == device_addr:
				device_addr_valid = True

		if device_addr_valid == False:
			print("[BLE] Invalid device address " + device_addr)
			return

		# if device address is valid, connect to it
		device = ble_connect(dev_addr=device_addr)
		if device == False:
			return

		# setup all the needed services
		util_service = device.getServiceByUUID(UUID(util_uuid))

		# setup the characteristics
		time_char = util_service.getCharacteristics(time_uuid)[0]
		csv_char = util_service.getCharacteristics(csv_uuid)[0]

		while True:
			try:
				board_time = struct.unpack("=l", time_char.read())[0]
				#batt_voltage = round(struct.unpack("f", batt_char.read())[0], 3)
				csv_string = csv_char.read().decode()
				devices_connected[device_addr] = time.strftime('%H:%M:%S', time.localtime(board_time)) + "; " + csv_string + "\n"
				if(verbose_output):
					print(devices_connected[device_addr])

			except Exception as e:
				print("[BLE] Disconnected from device", device_addr, "Trying to reconnect")
				devices_connected.pop(device_addr)
				device = ble_connect(device_addr)

				if device == False:
					print("[BLE] Could not reconnect to device", device_addr)
					break
				else:
					# setup all the needed services
					util_service = device.getServiceByUUID(UUID(util_uuid))

					# setup the characteristics
					time_char = util_service.getCharacteristics(time_uuid)[0]
					csv_char = util_service.getCharacteristics(csv_uuid)[0]

# Parse command line args
parser = argparse.ArgumentParser(description="Read data from a BLE Data Collection Board V1.0" )
parser.add_argument("-v", "--verbose", action="store_true", help="print any received data to stdout")
parser.add_argument("-d", "--devlist", type=str, help="Specify an input file containing MAC addresses of known boards. Lines with # are being ignored")
args = parser.parse_args()
verbose_output = args.verbose
mac_file_path = args.devlist

# If a device list was supplied as an arg, read it and parse out the mac addresses
if mac_file_path != None:
	mac_file = open(mac_file_path, "r")
	raw_content = mac_file.readlines()

	for line in raw_content:
		if not line.startswith("#"):
			devices_avail.append(line.split(" ")[0].strip("\n"))

	mac_file.close()
# If no device list was specified, discover nearby boards
else:
	try:
		print("No device list was passed, discovering boards automatically!")
		devices_avail = discover_boards(5).copy()
	except:
		print("[BLE] Failed to discover boards, did you run the script as root?")
		exit()

print("Successfully added", len(devices_avail), "devices to the list of known boards!")

# setup the TCP socket
tcp_ip = socket.gethostbyname(socket.gethostname())
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((tcp_ip, tcp_port))
sock.listen(1)
print("[TCP] Started listening on port", tcp_port, "address", tcp_ip)

# wait for new connections
while True:
	conn, addr = sock.accept()
	print("[TCP] Established connection to", addr)
	connection_thread = threading.Thread(target=connection_handler, args=(conn, addr), daemon=True)
	connection_thread.start()
