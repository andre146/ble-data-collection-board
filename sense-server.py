#!/usr/bin/python3

from bluepy.btle import UUID, Peripheral
import struct
import time
import socket
import threading
import argparse

#device address
device_addr = "E4:13:EA:5B:EE:54"
#device_addr = "0B:A2:EC:EF:7F:54"
#services
imu_uuid = "039343e0-7372-465c-9a6f-ffa139d18cfe"
hts_uuid = "04bd859d-d38a-4d8b-8314-3785accacd55"
util_uuid = "35cca5a5-ec0c-43ec-9cfe-3831f1ac3793"
#characteristics
acc_uuid_x = "039343e1-7372-465c-9a6f-ffa139d18cfe"
acc_uuid_y = "039343e2-7372-465c-9a6f-ffa139d18cfe"
acc_uuid_z = "039343e3-7372-465c-9a6f-ffa139d18cfe"

flux_uuid_x = "039343e4-7372-465c-9a6f-ffa139d18cfe"
flux_uuid_y = "039343e5-7372-465c-9a6f-ffa139d18cfe"
flux_uuid_z = "039343e6-7372-465c-9a6f-ffa139d18cfe"

temp_uuid = "04bd859e-d38a-4d8b-8314-3785accacd55"
time_uuid = "35cca5a7-ec0c-43ec-9cfe-3831f1ac3793"
batt_uuid = "35cca5a6-ec0c-43ec-9cfe-3831f1ac3793"
csv_uuid = "35cca5a8-ec0c-43ec-9cfe-3831f1ac3793"

#TCP settings
tcp_ip = '192.168.178.29'
tcp_port = 56254

#initialize all the available services and characteristics as strings
#they will later change to the apropriate objects
#The point of this is to be able to declare them as global variables in ble_value_reader()
device = ""
imu_service = ""
hts_service = ""
util_service = ""
acc_char_x = ""
acc_char_y = ""
acc_char_z = ""

flux_char_x = ""
flux_char_y = ""
flux_char_z = ""
temp_char = ""
time_char = ""
batt_char = ""
csv_char = ""

line_buffer = ""
buffer_lock = threading.Lock()
verbose_output = False

# try to connect to device and retry if connection failed
def ble_connect():
	global imu_service, hts_service, util_service
	global acc_char_x, acc_char_y, acc_char_z, flux_char_x, flux_char_y, flux_char_z, temp_char, time_char, batt_char, csv_char
	global acc_uuid_x, acc_uuid_y, acc_uuid_z, flux_uuid_x, flux_uuid_y, flux_uuid_z, temp_uuid, time_uuid, batt_uuid, csv_uuid
	global device

	while True:
		try:
			device = Peripheral(device_addr, "public")
			print("BLE: Connected to device", device_addr)
			break
		except:
			print("BLE: Could not connect to device", device_addr, "! retrying in 1 second")
			time.sleep(1)

	# setup all the available services
	imu_service = device.getServiceByUUID(UUID(imu_uuid))
	hts_service = device.getServiceByUUID(UUID(hts_uuid))
	util_service = device.getServiceByUUID(UUID(util_uuid))

	# setup all characteristics
	acc_char_x = imu_service.getCharacteristics(acc_uuid_x)[0]
	acc_char_y = imu_service.getCharacteristics(acc_uuid_y)[0]
	acc_char_z = imu_service.getCharacteristics(acc_uuid_z)[0]
	flux_char_x = imu_service.getCharacteristics(flux_uuid_x)[0]
	flux_char_y = imu_service.getCharacteristics(flux_uuid_y)[0]
	flux_char_z = imu_service.getCharacteristics(flux_uuid_z)[0]
	temp_char = hts_service.getCharacteristics(temp_uuid)[0]
	time_char = util_service.getCharacteristics(time_uuid)[0]
	batt_char = util_service.getCharacteristics(batt_uuid)[0]
	csv_char = util_service.getCharacteristics(csv_uuid)[0]

	# sync board time to local time
	time_char.write(struct.pack("=l", round(time.time())), withResponse=True)
	board_time = struct.unpack("=l", time_char.read())[0]
	if (time.time() - board_time) > 2:
		print("BLE: Error syncing board time to local time!")
		print("BLE: Board time is", board_time, ",Local time is", round(time.time()))
		device.disconnect()
		exit()
	else:
		print("BLE: Successfully synced board time to local time")

# handles incoming TCP connections
def connection_handler(conn, addr):
	global line_buffer
	line_buffer_old = ""

	try:
		conn.send(str("Timestamp; Acc X [g]; Acc Y [g]; Acc Z [g]; Flux X [µT]; Flux Y [µT]; Flux Z [µT]; Temp [°C]\n").encode('utf-8'))
		while True:
			#buffer_lock.acquire()
			if line_buffer_old != line_buffer:
				line_buffer_old = line_buffer
				conn.send(str(line_buffer).encode('utf-8'))
			#buffer_lock.release()
			time.sleep(0.5)
	except:
		print("TCP: Connection to", addr, "ended. Terminating handler")
		conn.close()
		#buffer_lock.release()

# reads values from the device over BLE
def ble_value_reader():
	global line_buffer
	global acc_char_x, acc_char_y, acc_char_z
	global flux_char_x, flux_char_y, flux_char_z
	global temp_char, time_char, batt_char

	# connect to the BLE device
	ble_connect()

	while True:
		try:
			# acc_x = round(struct.unpack("f", acc_char_x.read())[0], 3)
			# acc_y = round(struct.unpack("f", acc_char_y.read())[0], 3)
			# acc_z = round(struct.unpack("f", acc_char_z.read())[0], 3)
			# flux_x = round(struct.unpack("f", flux_char_x.read())[0], 3)
			# flux_y = round(struct.unpack("f", flux_char_y.read())[0], 3)
			# flux_z = round(struct.unpack("f", flux_char_z.read())[0], 3)
			#
			# temp = round(struct.unpack("f", temp_char.read())[0], 1)
			board_time = struct.unpack("=l", time_char.read())[0]
			#batt_voltage = round(struct.unpack("f", batt_char.read())[0], 3)
			csv_string = csv_char.read().decode()

			#buffer_lock.acquire()
			#line_buffer = time.strftime('%H:%M:%S', time.localtime(board_time)), "Acc: x", acc_x, "g y", acc_y, "g z", acc_z, \
			#			  "g Flux: x", flux_x, "µT y", flux_y, "µT z", flux_z, "µT Temp:", temp, "°C Batt Voltage:", batt_voltage
			line_buffer = time.strftime('%H:%M:%S', time.localtime(board_time)) + "; " + csv_string
			#buffer_lock.release()
			if(verbose_output):
				print(line_buffer)
		except:
			print("BLE: Disconnected from device! Trying to reconnect")
			ble_connect()

parser = argparse.ArgumentParser(description="Read data from a BLE Data Collection Board V1.0" )
parser.add_argument("-v", "--verbose", action="store_true", help="print any received data to stdout")
args = parser.parse_args()
verbose_output = args.verbose

# start the value reader in a separate thread
value_reader_thread = threading.Thread(target=ble_value_reader, daemon=True)
value_reader_thread.start()

# setup the TCP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((tcp_ip, tcp_port))
sock.listen(1)

# wait for new connections
while True:
	conn, addr = sock.accept()
	print("TCP: Established connection to", addr)
	connection_thread = threading.Thread(target=connection_handler, args=(conn, addr), daemon=True)
	connection_thread.start()

